0 -  Setup:
- install docker + docker-compose binaries
- install maven

1 - Build & Test:
- mvn clean verify

2 - Deploy:
- docker-compose -f system.docker-compose.yml up --build

3 - Access:
- HR-APP: http:localhost:6080/swagger-ui.html
- BOOKING-APP: http:localhost:7080/swagger-ui.html
- POSTGRES UI: http:localhost:8080
- KAFKA UI: http:localhost:9000

4 - Test (Round1):

! YOU SHALL CHANGE THE DATE IN THE PAYLOAD TO THE CURRENT DAY IF YOU WANT TO TEST ENTER/EXIT AS WELL !

! SCNEARIOS ARE DEFINED WITH THE PRESUMPTION OF EMPTY DATABASE !

Scenario 1: Initialize capacity to 10% and an employee register and enter and exit the office

curl -X POST "http://localhost:6080/hr/capacity" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"percentage\": 10}"

curl -X POST "http://localhost:7080/employee/register" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"date\": \"2020-06-16\", \"userId\": \"user\"}"

curl -X POST "http://localhost:7080/terminal/enter" -H "accept: */*" -H "Content-Type: application/json" -d "user"

curl -X POST "http://localhost:7080/terminal/exit" -H "accept: */*" -H "Content-Type: application/json" -d "user"

curl -X POST "http://localhost:7080/employee/status" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"date\": \"2020-06-16\", \"userId\": \"user\"}"

Scenario 2: Initialize capacity to 1% and 3 employees registers. The third one will have to wait. One exits. 3rd one can enter.

curl -X POST "http://localhost:6080/hr/capacity" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"percentage\": 1}"

curl -X POST "http://localhost:7080/employee/register" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"date\": \"2020-06-16\", \"userId\": \"user1\"}"

curl -X POST "http://localhost:7080/employee/register" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"date\": \"2020-06-16\", \"userId\": \"user2\"}"

curl -X POST "http://localhost:7080/employee/register" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"date\": \"2020-06-16\", \"userId\": \"user3\"}"

curl -X POST "http://localhost:7080/employee/status" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"date\": \"2020-06-16\", \"userId\": \"user3\"}"

curl -X POST "http://localhost:7080/terminal/enter" -H "accept: */*" -H "Content-Type: application/json" -d "user1"

curl -X POST "http://localhost:7080/terminal/exit" -H "accept: */*" -H "Content-Type: application/json" -d "user1"

curl -X POST "http://localhost:7080/employee/status" -H "accept: */*" -H "Content-Type: application/json" -d "{ \"date\": \"2020-06-16\", \"userId\": \"user3\"}"

curl -X POST "http://localhost:7080/terminal/enter" -H "accept: */*" -H "Content-Type: application/json" -d "user3"

