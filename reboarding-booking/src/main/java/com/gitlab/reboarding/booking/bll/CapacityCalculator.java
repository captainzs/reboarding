package com.gitlab.reboarding.booking.bll;

import com.gitlab.reboarding.booking.dal.CapacityRepository;
import com.gitlab.reboarding.common.models.CapacityEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CapacityCalculator {
    private final CapacityRepository repository;

    @Autowired
    public CapacityCalculator(CapacityRepository repository) {
        this.repository = repository;
    }

    public int getCurrentCapacity() {
        int capacity = 0;
        Optional<CapacityEntity> entity = this.repository.findTop1ByOrderByCreatedAtDesc();
        if (entity.isPresent()) {
            capacity = entity.get().getCapacity();
        }
        return capacity;
    }
}
