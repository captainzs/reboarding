package com.gitlab.reboarding.booking.bll;

import com.gitlab.reboarding.booking.dal.RegistrationRepository;
import com.gitlab.reboarding.booking.dal.VipRepository;
import com.gitlab.reboarding.booking.models.*;
import com.gitlab.reboarding.common.models.RegistrationEntity;
import com.gitlab.reboarding.common.models.SeatEntity;
import com.gitlab.reboarding.common.models.SeatStatus;
import com.gitlab.reboarding.common.models.VipEntity;
import com.gitlab.reboarding.common.opencv.ImageDrawer;
import com.gitlab.reboarding.common.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EmployeeService {
    private final CapacityCalculator capacityService;
    private final RegistrationRepository repository;
    private final SeatFinder seatFinder;
    private final ImageDrawer imageDrawer;
    private final VipRepository vipRepository;

    @Autowired
    public EmployeeService(CapacityCalculator capacityService, RegistrationRepository repository,
                           SeatFinder seatFinder, ImageDrawer imageDrawer, VipRepository vipRepository) {
        this.capacityService = capacityService;
        this.repository = repository;
        this.seatFinder = seatFinder;
        this.imageDrawer = imageDrawer;
        this.vipRepository = vipRepository;
    }

    @Retryable(
            value = {Throwable.class},
            maxAttempts = 10,
            backoff = @Backoff(random = true, delay = 500, maxDelay = 1500))
    @Transactional(propagation = Propagation.REQUIRES_NEW, isolation = Isolation.SERIALIZABLE)
    public DailyUserStatus register(RegisterDto dto) throws IllegalAttemptException {
        if (this.getVipNames().contains(dto.getUserId())) {
            throw new IllegalAttemptException("VIP member do not have to register!");
        }
        if (DateUtil.isDayInPast(dto.getDate())) {
            throw new IllegalAttemptException("Can not register for a day in the past!");
        }
        DailyUserStatus currentStatus = this.getStatus(dto);
        if (!currentStatus.canRegister()) {
            throw new IllegalAttemptException(String.format("User %s can not register again for this day %s!", dto.getUserId(), dto.getDate()));
        }
        Optional<SeatEntity> freeSeat = this.seatFinder.findAvailableSeat(dto.getDate());
        if (!freeSeat.isPresent()) {
            throw new IllegalAttemptException(String.format("User %s can not register because no more seat available!", dto.getUserId()));
        }
        this.repository.save(dto.toEntity(freeSeat.get(), DateUtil.now()));
        return this.getStatus(dto);
    }

    public DailyUserStatus getStatus(RegisterDto dto) {
        if (this.getVipNames().contains(dto.getUserId())) {
            return new VipStatus(dto.getUserId(), dto.getDate());
        }
        List<RegistrationEntity> registrations = this.repository.findSomeByUserIdAndDay(dto.getUserId(), dto.getDate(),
                PageRequest.of(0, 1, Sort.by(Sort.Direction.DESC, "createdAt")));
        if (registrations.size() == 0) {
            return new NonRegisteredStatus(dto.getUserId(), dto.getDate());
        }
        RegistrationEntity reg = registrations.get(0);
        int capacity = this.capacityService.getCurrentCapacity();
        long position = this.repository.countActivesByDayAndByBeforeTime(reg.getRegisteredTo(), reg.getCreatedAt()) + 1;
        if (position > capacity) {
            return new WaitListStatus(reg, position - capacity);
        }
        return new AcceptedStatus(reg);
    }

    public ByteArrayResource getSeatImage(Long registrationId) throws IllegalAttemptException, IOException {
        Optional<RegistrationEntity> registration = this.repository.findById(registrationId);
        if (!registration.isPresent()) {
            throw new IllegalAttemptException(String.format("Registration with id %d not exists!", registrationId));
        }
        return this.imageDrawer.createOfficeMapWithSeatStatus(registration.get().getReservedSeat(), SeatStatus.REGISTERED);
    }

    private Set<String> getVipNames() {
        return this.vipRepository.findAll().stream().map(VipEntity::getName).collect(Collectors.toSet());
    }
}
