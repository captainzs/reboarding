package com.gitlab.reboarding.booking.bll;

import com.gitlab.reboarding.booking.dal.SeatRepository;
import com.gitlab.reboarding.common.models.SeatEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.Date;
import java.util.Optional;

@Component
public class SeatFinder {
    private final SeatRepository repository;

    @Autowired
    public SeatFinder(SeatRepository repository) {
        this.repository = repository;
    }

    public Optional<SeatEntity> findAvailableSeat(Date day) {
        return this.repository.findAll().stream()
                .filter(c -> c.isFree(day))
                .filter(c -> c.isInSafeArea(day))
                .min(Comparator.comparingInt(c -> c.safeDegree(day)));
    }
}
