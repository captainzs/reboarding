package com.gitlab.reboarding.booking.bll;

import com.gitlab.reboarding.booking.comm.KafkaEventSender;
import com.gitlab.reboarding.booking.dal.EnterEventRepository;
import com.gitlab.reboarding.booking.dal.ExitEventRepository;
import com.gitlab.reboarding.booking.dal.RegistrationRepository;
import com.gitlab.reboarding.booking.models.AcceptedStatus;
import com.gitlab.reboarding.booking.models.DailyUserStatus;
import com.gitlab.reboarding.booking.models.IllegalAttemptException;
import com.gitlab.reboarding.booking.models.RegisterDto;
import com.gitlab.reboarding.common.models.EnterEventEntity;
import com.gitlab.reboarding.common.models.ExitEventEntity;
import com.gitlab.reboarding.common.models.RegistrationEntity;
import com.gitlab.reboarding.common.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;

@Service
public class TerminalService {
    private final CapacityCalculator capacityService;
    private final EmployeeService employeeService;
    private final RegistrationRepository registrationRepository;
    private final EnterEventRepository enterEventRepository;
    private final ExitEventRepository exitEventRepository;
    private final KafkaEventSender kafkaEventSender;

    @Autowired
    public TerminalService(CapacityCalculator capacityService, EmployeeService employeeService,
                           RegistrationRepository registrationRepository, EnterEventRepository eventRepository,
                           ExitEventRepository exitEventRepository, KafkaEventSender kafkaEventSender) {
        this.capacityService = capacityService;
        this.employeeService = employeeService;
        this.registrationRepository = registrationRepository;
        this.enterEventRepository = eventRepository;
        this.exitEventRepository = exitEventRepository;
        this.kafkaEventSender = kafkaEventSender;
    }

    public void enter(String userId) throws IllegalAttemptException {
        Date now = DateUtil.now();
        RegisterDto dto = new RegisterDto(userId, DateUtil.now());
        DailyUserStatus status = this.employeeService.getStatus(dto);
        if (!status.canEnter()) {
            throw new IllegalAttemptException(String.format("User %s can not enter, possibly because has not registered yet!", userId));
        }
        if (status instanceof AcceptedStatus) {
            AcceptedStatus registeredStatus = (AcceptedStatus) status;
            this.enterEventRepository.saveAndFlush(new EnterEventEntity(registeredStatus.getRegistration(), now));
        }
    }

    @Transactional
    public void exit(String userId) throws IllegalAttemptException {
        Date now = DateUtil.now();
        RegisterDto dto = new RegisterDto(userId, DateUtil.now());
        DailyUserStatus status = this.employeeService.getStatus(dto);
        if (!status.canExit()) {
            throw new IllegalAttemptException(String.format("User %s can not exit, possibly because has not entered yet!", userId));
        }
        if (status instanceof AcceptedStatus) {
            int capacity = this.capacityService.getCurrentCapacity();
            List<RegistrationEntity> registrations = this.registrationRepository.findSomeActivesByDay(now,
                    PageRequest.of(capacity + 3, 1, Sort.Direction.ASC, "createdAt"));
            if (registrations.size() == 1) {
                this.kafkaEventSender.sendEvent(registrations.get(0).getUserId());
            }
            AcceptedStatus registeredStatus = (AcceptedStatus) status;
            //TODO jo lenne ha exiteles utan lenne csak kafka
            this.exitEventRepository.saveAndFlush(new ExitEventEntity(registeredStatus.getRegistration().getEnterEvent(), now));
        }
    }
}
