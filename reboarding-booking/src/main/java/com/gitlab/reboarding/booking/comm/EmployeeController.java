package com.gitlab.reboarding.booking.comm;

import com.gitlab.reboarding.booking.bll.EmployeeService;
import com.gitlab.reboarding.booking.models.DailyUserStatus;
import com.gitlab.reboarding.booking.models.IllegalAttemptException;
import com.gitlab.reboarding.booking.models.RegisterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {
    private final EmployeeService service;

    @Autowired
    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @PostMapping(value = "/register")
    public DailyUserStatus register(@RequestBody @Valid RegisterDto dto) throws IllegalAttemptException {
        return this.service.register(dto);
    }

    @PostMapping(value = "/status")
    public DailyUserStatus status(@RequestBody @Valid RegisterDto dto) {
        return this.service.getStatus(dto);
    }

    @GetMapping(value = "/registration/{regid}")
    public ResponseEntity<Resource> getSeatImage(@PathVariable("regid") Long registrationId) throws IllegalAttemptException, IOException {
        ByteArrayResource resource = this.service.getSeatImage(registrationId);
        return ResponseEntity.ok()
                .contentLength(resource.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }
}
