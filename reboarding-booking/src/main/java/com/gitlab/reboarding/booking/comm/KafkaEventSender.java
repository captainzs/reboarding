package com.gitlab.reboarding.booking.comm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

@Component
@EnableBinding(Source.class)
public class KafkaEventSender {
    private final Source source;

    @Autowired
    public KafkaEventSender(Source source) {
        this.source = source;
    }

    public void sendEvent(String userId) {
        this.source.output().send(MessageBuilder.withPayload(userId).build());
    }
}
