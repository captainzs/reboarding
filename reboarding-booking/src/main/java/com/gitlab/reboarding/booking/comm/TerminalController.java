package com.gitlab.reboarding.booking.comm;

import com.gitlab.reboarding.booking.bll.TerminalService;
import com.gitlab.reboarding.booking.models.IllegalAttemptException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/terminal")
public class TerminalController {
    private final TerminalService service;

    @Autowired
    public TerminalController(TerminalService service) {
        this.service = service;
    }

    @PostMapping(value = "/enter")
    public void enter(@RequestBody String userId) throws IllegalAttemptException {
        this.service.enter(userId);
    }

    @PostMapping(value = "/exit")
    public void exit(@RequestBody String userId) throws IllegalAttemptException {
        this.service.exit(userId);
    }
}
