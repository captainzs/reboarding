package com.gitlab.reboarding.booking.dal;

import com.gitlab.reboarding.common.models.CapacityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("BookingCapacityRepository")
public interface CapacityRepository extends JpaRepository<CapacityEntity, Long> {
    Optional<CapacityEntity> findTop1ByOrderByCreatedAtDesc();
}
