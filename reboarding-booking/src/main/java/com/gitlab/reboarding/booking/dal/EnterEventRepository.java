package com.gitlab.reboarding.booking.dal;

import com.gitlab.reboarding.common.models.EnterEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface EnterEventRepository extends JpaRepository<EnterEventEntity, Long> {
    @Query("SELECT e FROM EnterEventEntity e WHERE e.registration.userId=?1 AND e.registration.registeredTo=?2")
    Optional<EnterEventEntity> findEnterEvent(String userId, Date day);
}
