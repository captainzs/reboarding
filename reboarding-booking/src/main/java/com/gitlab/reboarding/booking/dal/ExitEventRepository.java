package com.gitlab.reboarding.booking.dal;

import com.gitlab.reboarding.common.models.ExitEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExitEventRepository extends JpaRepository<ExitEventEntity, Long> {
}
