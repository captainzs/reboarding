package com.gitlab.reboarding.booking.dal;

import com.gitlab.reboarding.common.models.RegistrationEntity;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository("BookingRegistrationRepository")
public interface RegistrationRepository extends JpaRepository<RegistrationEntity, Long> {
    @Query("SELECT COUNT(r) FROM RegistrationEntity r LEFT JOIN r.enterEvent e LEFT JOIN e.exitEvent x WHERE r.registeredTo=?1 AND r.createdAt<?2 AND (e.id IS NULL OR x.id IS NULL)")
    Long countActivesByDayAndByBeforeTime(Date onDay, Date before);

    @Query("SELECT r FROM RegistrationEntity r WHERE r.userId=?1 AND r.registeredTo=?2")
    List<RegistrationEntity> findSomeByUserIdAndDay(String userId, Date registeredTo, Pageable page);

    @Query("SELECT r FROM RegistrationEntity r LEFT JOIN r.enterEvent e LEFT JOIN e.exitEvent x WHERE r.registeredTo=?1 AND (e.id IS NULL OR x.id IS NULL)")
    List<RegistrationEntity> findSomeActivesByDay(Date onDay, Pageable page);

    @Query("SELECT r FROM RegistrationEntity r LEFT JOIN r.enterEvent e LEFT JOIN e.exitEvent x WHERE r.registeredTo=?1 AND (e.id IS NULL OR x.id IS NULL)")
    List<RegistrationEntity> findAllActivesByDay(Date onDay);
}
