package com.gitlab.reboarding.booking.dal;

import com.gitlab.reboarding.common.models.SeatEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("BookingSeatRepository")
public interface SeatRepository extends JpaRepository<SeatEntity, Long> {
}
