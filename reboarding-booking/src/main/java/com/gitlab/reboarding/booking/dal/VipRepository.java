package com.gitlab.reboarding.booking.dal;

import com.gitlab.reboarding.common.models.VipEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("BookingVipRepository")
public interface VipRepository extends JpaRepository<VipEntity, Long> {
}
