package com.gitlab.reboarding.booking.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.gitlab.reboarding.common.models.RegistrationEntity;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;

public class AcceptedStatus extends DailyUserStatus {
    private final RegistrationEntity registration;

    public AcceptedStatus(RegistrationEntity registration) {
        super(registration.getUserId(), registration.getRegisteredTo());
        this.registration = registration;
    }

    @JsonIgnore
    public RegistrationEntity getRegistration() {
        return this.registration;
    }

    @Override
    public boolean canRegister() {
        return this.registration.getEnterEvent() != null && this.registration.getEnterEvent().getExitEvent() != null;
    }

    @Override
    public boolean canEnter() {
        return this.registration.getEnterEvent() == null;
    }

    @Override
    public boolean canExit() {
        return this.registration.getEnterEvent() != null && this.registration.getEnterEvent().getExitEvent() == null;
    }

    @JsonProperty(value = "seatId")
    public Long getSeatId() {
        return this.registration.getReservedSeat().getId();
    }

    @JsonProperty(value = "seatDownloadUrl")
    public URI seatDownloadUrl() {
        return ServletUriComponentsBuilder.fromCurrentContextPath()
                .pathSegment("employee")
                .pathSegment("registration")
                .pathSegment(this.registration.getId().toString())
                .build().toUri();
    }
}
