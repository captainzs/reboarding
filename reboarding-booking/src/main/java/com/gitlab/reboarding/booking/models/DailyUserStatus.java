package com.gitlab.reboarding.booking.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gitlab.reboarding.common.utils.DayDeserializer;
import com.gitlab.reboarding.common.utils.DaySerializer;

import java.util.Date;

public abstract class DailyUserStatus {
    private final String userId;
    private final Date day;

    protected DailyUserStatus(String userId, Date day) {
        this.userId = userId;
        this.day = day;
    }

    public String getUserId() {
        return this.userId;
    }

    @JsonSerialize(using = DaySerializer.class)
    @JsonDeserialize(using = DayDeserializer.class)
    public Date getDay() {
        return this.day;
    }

    @JsonProperty(value = "canRegister")
    public abstract boolean canRegister();

    @JsonProperty(value = "canEnter")
    public abstract boolean canEnter();

    @JsonProperty(value = "canExit")
    public abstract boolean canExit();
}
