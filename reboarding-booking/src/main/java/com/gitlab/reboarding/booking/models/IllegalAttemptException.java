package com.gitlab.reboarding.booking.models;

public class IllegalAttemptException extends Exception {
    public IllegalAttemptException(String message) {
        super(message);
    }
}
