package com.gitlab.reboarding.booking.models;

import java.util.Date;

public class NonRegisteredStatus extends DailyUserStatus {
    public NonRegisteredStatus(String userId, Date day) {
        super(userId, day);
    }

    @Override
    public boolean canRegister() {
        return true;
    }

    @Override
    public boolean canEnter() {
        return false;
    }

    @Override
    public boolean canExit() {
        return false;
    }
}
