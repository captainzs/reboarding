package com.gitlab.reboarding.booking.models;

import com.gitlab.reboarding.common.models.RegistrationEntity;
import com.gitlab.reboarding.common.models.SeatEntity;

import javax.validation.constraints.NotNull;
import java.util.Date;

public class RegisterDto {
    @NotNull
    private final String userId;
    @NotNull
    private final Date date;

    public RegisterDto(String userId, Date date) {
        this.userId = userId;
        this.date = date;
    }

    public String getUserId() {
        return this.userId;
    }

    public Date getDate() {
        return this.date;
    }

    public RegistrationEntity toEntity(SeatEntity seat, Date registeredAt) {
        return new RegistrationEntity(seat, this.userId, this.date, registeredAt);
    }
}
