package com.gitlab.reboarding.booking.models;

import java.util.Date;

public class VipStatus extends DailyUserStatus {
    public VipStatus(String userId, Date day) {
        super(userId, day);
    }

    @Override
    public boolean canRegister() {
        return false;
    }

    @Override
    public boolean canEnter() {
        return true;
    }

    @Override
    public boolean canExit() {
        return true;
    }
}
