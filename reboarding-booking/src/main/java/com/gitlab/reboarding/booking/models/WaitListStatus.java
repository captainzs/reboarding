package com.gitlab.reboarding.booking.models;

import com.gitlab.reboarding.common.models.RegistrationEntity;

public class WaitListStatus extends AcceptedStatus {
    private final Long waitNo;

    public WaitListStatus(RegistrationEntity registration, Long waitNo) {
        super(registration);
        this.waitNo = waitNo;
    }

    public Long getWaitNo() {
        return this.waitNo;
    }

    @Override
    public boolean canRegister() {
        return false;
    }

    @Override
    public boolean canEnter() {
        return false;
    }

    @Override
    public boolean canExit() {
        return false;
    }
}
