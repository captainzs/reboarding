package com.gitlab.reboarding.booking.comm;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.reboarding.booking.dal.SeatRepository;
import com.gitlab.reboarding.booking.models.RegisterDto;
import com.gitlab.reboarding.common.models.SeatEntity;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Date;


@SpringBootTest(properties = {
        "server.port=7080",
        "spring.datasource.url=jdbc:h2:mem:testdb",
        "spring.datasource.driverClassName=org.h2.Driver",
        "spring.datasource.username=sa",
        "spring.datasource.password=sa",
        "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect",
        "spring.jpa.hibernate.ddl-auto=create",
        "spring.cloud.stream.bindings.output.destination=events"})
@AutoConfigureMockMvc
@EmbeddedKafka(ports = 9092)
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class EmployeeControllerIT {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private SeatRepository repository;

    @Before
    public void before() {
        this.repository.save(new SeatEntity(100, 50));
    }

    @Test
    public void givenUserAndDay_whenRegisterTwice_assertFirstSuccessSecondFail() throws Exception {
        RegisterDto input = new RegisterDto("UserId", new Date());
        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(input)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(input)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void givenUserAndDay_whenRegister_assertSuccessAndStatusOk() throws Exception {
        String userId = "UserId";
        RegisterDto input = new RegisterDto(userId, new Date());
        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(input)))
                .andExpect(MockMvcResultMatchers.status().isOk());
        MvcResult result = this.mockMvc.perform(
                MockMvcRequestBuilders.post("/employee/status")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(input)))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        String resultStrJson = result.getResponse().getContentAsString();
        JSONObject jsonObject = new JSONObject(resultStrJson);
        Assert.assertEquals(userId, jsonObject.getString("userId"));
        Assert.assertEquals(1, jsonObject.getInt("waitNo"));
        Assert.assertFalse(jsonObject.getBoolean("canRegister"));
        Assert.assertFalse(jsonObject.getBoolean("canExit"));
        Assert.assertFalse(jsonObject.getBoolean("canEnter"));
    }
}
