package com.gitlab.reboarding.booking.comm;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@SpringBootTest(properties = {
        "server.port=7080",
        "spring.datasource.url=jdbc:h2:mem:testdb",
        "spring.datasource.driverClassName=org.h2.Driver",
        "spring.datasource.username=sa",
        "spring.datasource.password=sa",
        "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect",
        "spring.jpa.hibernate.ddl-auto=create"})
@AutoConfigureMockMvc
@EmbeddedKafka(ports = 9092)
@RunWith(SpringJUnit4ClassRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TerminalControllerIT {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void givenNoRegistration_whenEnter_assertForbidden() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/terminal/enter").content("UserId"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void givenNoRegistration_whenExit_assertForbidden() throws Exception {
        this.mockMvc.perform(
                MockMvcRequestBuilders.post("/terminal/exit").content("UserId"))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }
}
