package com.gitlab.reboarding.booking.bll;

import com.gitlab.reboarding.booking.dal.CapacityRepository;
import com.gitlab.reboarding.common.models.CapacityEntity;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
public class CapacityServiceTest {
    private CapacityCalculator objectUnderTest;

    @Mock
    private CapacityRepository repository;

    @Before
    public void setUp() {
        this.objectUnderTest = new CapacityCalculator(this.repository);
    }

    @Test
    public void givenCapacity_whenGetCurrent_assertGivenReturned() {
        int capacitySize = 10;
        CapacityEntity capacity = Mockito.mock(CapacityEntity.class);
        Mockito.when(capacity.getCapacity()).thenReturn(capacitySize);
        Mockito.when(this.repository.findTop1ByOrderByCreatedAtDesc()).thenReturn(Optional.of(capacity));
        Assert.assertEquals(capacitySize, this.objectUnderTest.getCurrentCapacity());
    }

    @Test
    public void givenNoCapacity_whenGetCurrent_assert0Returned() {
        Mockito.when(this.repository.findTop1ByOrderByCreatedAtDesc()).thenReturn(Optional.empty());
        Assert.assertEquals(0, this.objectUnderTest.getCurrentCapacity());
    }
}
