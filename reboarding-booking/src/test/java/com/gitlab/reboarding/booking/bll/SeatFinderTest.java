package com.gitlab.reboarding.booking.bll;

import com.gitlab.reboarding.booking.dal.SeatRepository;
import com.gitlab.reboarding.common.models.SeatEntity;
import com.gitlab.reboarding.common.utils.DateUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@RunWith(MockitoJUnitRunner.class)
public class SeatFinderTest {
    private SeatFinder objectUnderTest;

    @Mock
    private SeatRepository repository;

    @Before
    public void setUp() {
        this.objectUnderTest = new SeatFinder(this.repository);
    }

    @Test
    public void given2SeatsWithSameDegree_whenSelectSeat_assertOneOfThemReturned() {
        List<SeatEntity> seats = new ArrayList<>();
        SeatEntity s1 = Mockito.mock(SeatEntity.class);
        Mockito.when(s1.isFree(Mockito.any())).thenReturn(Boolean.TRUE);
        Mockito.when(s1.isInSafeArea(Mockito.any())).thenReturn(Boolean.TRUE);
        Mockito.when(s1.safeDegree(Mockito.any())).thenReturn(1);
        SeatEntity s2 = Mockito.mock(SeatEntity.class);
        Mockito.when(s2.isFree(Mockito.any())).thenReturn(Boolean.TRUE);
        Mockito.when(s2.isInSafeArea(Mockito.any())).thenReturn(Boolean.TRUE);
        Mockito.when(s2.safeDegree(Mockito.any())).thenReturn(1);
        seats.add(s1);
        seats.add(s2);

        Mockito.when(this.repository.findAll()).thenReturn(seats);
        Optional<SeatEntity> seat = this.objectUnderTest.findAvailableSeat(DateUtil.now());
        Assert.assertTrue(seat.isPresent());
    }
}
