package com.gitlab.reboarding.booking.models;

import com.gitlab.reboarding.common.models.EnterEventEntity;
import com.gitlab.reboarding.common.models.ExitEventEntity;
import com.gitlab.reboarding.common.models.RegistrationEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;


@RunWith(JUnit4.class)
public class RegisteredStatusTest {

    @Test
    public void givenRegistration_whenGetters_assertSame() {
        RegistrationEntity reg = Mockito.mock(RegistrationEntity.class);
        AcceptedStatus status = new AcceptedStatus(reg);
        Assert.assertEquals(reg, status.getRegistration());
    }

    @Test
    public void givenRegistrationWithoutEnter_whenCanEnter_assertCanEnter() {
        RegistrationEntity reg = Mockito.mock(RegistrationEntity.class);
        AcceptedStatus status = new AcceptedStatus(reg);
        Assert.assertTrue(status.canEnter());
        Assert.assertFalse(status.canExit());
        Assert.assertFalse(status.canRegister());
    }

    @Test
    public void givenRegistrationWithEnter_whenCans_assertCanNotEnterAndCanExit() {
        RegistrationEntity reg = Mockito.mock(RegistrationEntity.class);
        Mockito.when(reg.getEnterEvent()).thenReturn(Mockito.mock(EnterEventEntity.class));
        AcceptedStatus status = new AcceptedStatus(reg);
        Assert.assertFalse(status.canEnter());
        Assert.assertTrue(status.canExit());
        Assert.assertFalse(status.canRegister());
    }

    @Test
    public void givenRegistrationWithEnterAndExit_whenCans_assertCanNotEnterAndCanNotExitAndCanReg() {
        RegistrationEntity reg = Mockito.mock(RegistrationEntity.class);
        EnterEventEntity enter = Mockito.mock(EnterEventEntity.class);
        Mockito.when(reg.getEnterEvent()).thenReturn(enter);
        Mockito.when(enter.getExitEvent()).thenReturn(Mockito.mock(ExitEventEntity.class));
        AcceptedStatus status = new AcceptedStatus(reg);
        Assert.assertFalse(status.canEnter());
        Assert.assertFalse(status.canExit());
        Assert.assertTrue(status.canRegister());
    }
}
