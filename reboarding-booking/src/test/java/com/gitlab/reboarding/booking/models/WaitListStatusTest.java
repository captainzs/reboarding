package com.gitlab.reboarding.booking.models;

import com.gitlab.reboarding.common.models.RegistrationEntity;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;


@RunWith(JUnit4.class)
public class WaitListStatusTest {

    @Test
    public void givenWaitNoAndRegistration_whenGetters_assertNothingCanBeDone() {
        long waitNo = 10L;
        RegistrationEntity reg = Mockito.mock(RegistrationEntity.class);
        WaitListStatus status = new WaitListStatus(reg, waitNo);
        Assert.assertEquals(waitNo, status.getWaitNo().longValue());
        Assert.assertFalse(status.canRegister());
        Assert.assertFalse(status.canExit());
        Assert.assertFalse(status.canEnter());
    }
}
