package com.gitlab.reboarding.common.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CapacityEvent")
public class CapacityEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Capacity", nullable = false, unique = false, updatable = true)
    private Integer capacity;

    @Column(name = "CreatedAt", nullable = false, unique = false, updatable = false)
    private Date createdAt;

    private CapacityEntity() {
    }

    public CapacityEntity(Integer capacity, Date createdAt) {
        this.capacity = capacity;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return this.id;
    }

    public Integer getCapacity() {
        return this.capacity;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }
}
