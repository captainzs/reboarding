package com.gitlab.reboarding.common.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "EnterEvent")
public class EnterEventEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "RegistrationId", referencedColumnName = "Id", nullable = false, unique = true, updatable = false)
    private RegistrationEntity registration;

    @OneToOne(mappedBy = "enterEvent")
    private ExitEventEntity exitEvent;

    @Column(name = "CreatedAt", nullable = false, unique = false, updatable = false)
    private Date createdAt;

    public EnterEventEntity() {
    }

    public EnterEventEntity(RegistrationEntity registration, Date createdAt) {
        this.registration = registration;
        this.createdAt = createdAt;
    }

    public RegistrationEntity getRegistration() {
        return this.registration;
    }

    public ExitEventEntity getExitEvent() {
        return this.exitEvent;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }
}
