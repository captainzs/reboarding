package com.gitlab.reboarding.common.models;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ExitEvent")
public class ExitEventEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @OneToOne
    @JoinColumn(name = "EnterEventId", referencedColumnName = "Id", nullable = false, unique = true, updatable = false)
    private EnterEventEntity enterEvent;

    @Column(name = "CreatedAt", nullable = false, unique = false, updatable = false)
    private Date createdAt;

    public ExitEventEntity() {
    }

    public ExitEventEntity(EnterEventEntity enterEvent, Date createdAt) {
        this.enterEvent = enterEvent;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return this.id;
    }

    public EnterEventEntity getEnterEvent() {
        return this.enterEvent;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }
}
