package com.gitlab.reboarding.common.models;

import com.gitlab.reboarding.common.utils.DayConverter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "Registration", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"RegisteredTo", "CreatedAt"})
})
public class RegistrationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @OneToOne(mappedBy = "registration")
    private EnterEventEntity enterEvent;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SeatId", referencedColumnName = "Id", nullable = false, unique = false, updatable = false)
    private SeatEntity reservedSeat;

    @Column(name = "UserId", nullable = false, unique = false, updatable = false)
    private String userId;

    @Convert(converter = DayConverter.class)
    @Column(name = "RegisteredTo", nullable = false, unique = false, updatable = false)
    private Date registeredTo;

    @Column(name = "CreatedAt", nullable = false, unique = false, updatable = false)
    private Date createdAt;

    private RegistrationEntity() {
    }

    public RegistrationEntity(SeatEntity reservedSeat, String userId, Date registeredTo, Date createdAt) {
        this.reservedSeat = reservedSeat;
        this.userId = userId;
        this.registeredTo = registeredTo;
        this.createdAt = createdAt;
    }

    public Long getId() {
        return this.id;
    }

    public SeatEntity getReservedSeat() {
        return this.reservedSeat;
    }

    public EnterEventEntity getEnterEvent() {
        return this.enterEvent;
    }

    public String getUserId() {
        return this.userId;
    }

    public Date getRegisteredTo() {
        return this.registeredTo;
    }

    public Date getCreatedAt() {
        return this.createdAt;
    }

    public boolean isEntered() {
        return this.enterEvent != null;
    }

    public boolean isExited() {
        return this.isEntered() && this.enterEvent.getExitEvent() != null;
    }
}
