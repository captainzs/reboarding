package com.gitlab.reboarding.common.models;

import com.gitlab.reboarding.common.utils.DateUtil;

import javax.persistence.*;
import java.util.Date;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "Seat")
public class SeatEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "PosX", nullable = false, unique = false, updatable = false)
    private Double posX;

    @Column(name = "PosY", nullable = false, unique = false, updatable = false)
    private Double posY;

    @ManyToMany
    @JoinTable(
            name = "SeatConflict",
            joinColumns = @JoinColumn(name = "SeatId"),
            inverseJoinColumns = @JoinColumn(name = "InConflictWithSeatId"))
    private Set<SeatEntity> inConflictWith;

    @OneToMany(mappedBy = "reservedSeat", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<RegistrationEntity> registrations;

    public SeatEntity() {
    }

    public SeatEntity(double posX, double posY) {
        this.posX = posX;
        this.posY = posY;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPosX() {
        return this.posX;
    }

    public void setPosX(Double posX) {
        this.posX = posX;
    }

    public Double getPosY() {
        return this.posY;
    }

    public void setPosY(Double posY) {
        this.posY = posY;
    }

    public Set<SeatEntity> getInConflictWith() {
        return this.inConflictWith;
    }

    public void setInConflictWith(Set<SeatEntity> inConflictWith) {
        this.inConflictWith = inConflictWith;
    }

    public Optional<RegistrationEntity> getActiveRegistration(Date day) {
        return this.registrations.stream().filter(r -> DateUtil.isDameDay(r.getRegisteredTo(), day)).filter(r -> !r.isExited()).findAny();
    }

    public boolean isFree(Date day) {
        return !this.getActiveRegistration(day).isPresent();
    }

    public boolean isInSafeArea(Date day) {
        return this.inConflictWith.stream().allMatch(c -> c.isFree(day));
    }

    public int safeDegree(Date day) {
        return (int) this.inConflictWith.stream().filter(c -> c.isInSafeArea(day)).count();
    }
}
