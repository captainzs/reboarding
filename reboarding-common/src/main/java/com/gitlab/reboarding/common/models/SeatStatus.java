package com.gitlab.reboarding.common.models;

public enum SeatStatus {
    RESERVED, REGISTERED, FREE
}
