package com.gitlab.reboarding.common.models;

import javax.persistence.*;

@Entity
@Table(name = "VipMember")
public class VipEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Name", nullable = false, unique = false, updatable = false)
    private String name;

    public VipEntity() {
    }

    public VipEntity(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
