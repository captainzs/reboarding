package com.gitlab.reboarding.common.opencv;

import com.gitlab.reboarding.common.models.SeatEntity;
import com.gitlab.reboarding.common.models.SeatStatus;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.springframework.core.io.ByteArrayResource;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ImageDrawer {
    private final OfficeImageLoader imageLoader;

    public ImageDrawer(OfficeImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    public ByteArrayResource createOfficeMapWithSeatStatus(SeatEntity seat, SeatStatus status) throws IOException {
        Map<SeatEntity, SeatStatus> statuses = new HashMap<>();
        statuses.put(seat, status);
        return this.createOfficeMapWithSeatStatuses(statuses);
    }

    public ByteArrayResource createOfficeMapWithSeatStatuses(Map<SeatEntity, SeatStatus> seats) throws IOException {
        Mat layout = this.imageLoader.loadLayout();
        for (Map.Entry<SeatEntity, SeatStatus> seatStatus : seats.entrySet()) {
            Scalar color = null;
            switch (seatStatus.getValue()) {
                case FREE:
                    color = new Scalar(0, 255, 0);
                    break;
                case REGISTERED:
                    color = new Scalar(0, 255, 255);
                    break;
                case RESERVED:
                    color = new Scalar(0, 0, 255);
                    break;
            }
            Imgproc.circle(layout, new Point(seatStatus.getKey().getPosX(), seatStatus.getKey().getPosY()), 4, color, -1);
        }
        MatOfByte matOfByte = new MatOfByte();
        Imgcodecs.imencode(".jpg", layout, matOfByte);
        byte[] byteArray = matOfByte.toArray();
        return new ByteArrayResource(byteArray);
    }
}
