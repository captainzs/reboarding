package com.gitlab.reboarding.common.opencv;

import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;

public class OfficeImageLoader {
    private static final String LAYOUT_FILE_NAME = "original_layout.jpg";
    private static final String HORIZONTAL_TEMPLATE_FILE_NAME = "template_horizontal.png";
    private static final String VERTICAL_TEMPLATE_FILE_NAME = "template_vertical.png";

    public Mat loadLayout() throws IOException {
        return this.loadResource(LAYOUT_FILE_NAME);
    }

    public Mat loadHorizontalTemplate() throws IOException {
        return this.loadResource(HORIZONTAL_TEMPLATE_FILE_NAME);
    }

    public Mat loadVerticalTemplate() throws IOException {
        return this.loadResource(VERTICAL_TEMPLATE_FILE_NAME);
    }

    private Mat loadResource(String fileName) throws IOException {
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream(fileName);
        byte[] data = StreamUtils.copyToByteArray(stream);
        return Imgcodecs.imdecode(new MatOfByte(data), Imgcodecs.IMREAD_UNCHANGED);
    }
}
