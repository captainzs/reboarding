package com.gitlab.reboarding.common.utils;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class DateUtil {
    public static boolean isDayInPast(Date date) {
        ZonedDateTime givenTime = date.toInstant().atZone(ZoneId.systemDefault());
        ZonedDateTime now = ZonedDateTime.now(ZoneId.systemDefault());
        return givenTime.getYear() < now.getYear() ||
                (givenTime.getYear() == now.getYear() && givenTime.getDayOfYear() < now.getDayOfYear());
    }

    public static boolean isDameDay(Date day, Date otherDay) {
        ZonedDateTime dayTime = day.toInstant().atZone(ZoneId.systemDefault());
        ZonedDateTime otherDayTime = otherDay.toInstant().atZone(ZoneId.systemDefault());
        return dayTime.getYear() == otherDayTime.getYear() && dayTime.getDayOfYear() == otherDayTime.getDayOfYear();
    }

    public static Date now() {
        return Date.from(ZonedDateTime.now(ZoneId.systemDefault()).toInstant());
    }
}
