package com.gitlab.reboarding.common.utils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Date;

@Converter
public class DayConverter implements AttributeConverter<Date, String> {
    private final DaySerializer serializer;
    private final DayDeserializer deserializer;

    public DayConverter() {
        this.serializer = new DaySerializer();
        this.deserializer = new DayDeserializer();
    }

    public DayConverter(DaySerializer serializer, DayDeserializer deserializer) {
        this.serializer = serializer;
        this.deserializer = deserializer;
    }

    @Override
    public String convertToDatabaseColumn(Date date) {
        return this.serializer.serialize(date);
    }

    @Override
    public Date convertToEntityAttribute(String dayOfYear) {
        return this.deserializer.deserialize(dayOfYear);
    }
}
