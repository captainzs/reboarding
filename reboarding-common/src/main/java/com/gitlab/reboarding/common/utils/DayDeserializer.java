package com.gitlab.reboarding.common.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DayDeserializer extends JsonDeserializer<Date> {
    @Override
    public Date deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        return this.deserialize(jsonParser.readValueAs(String.class));
    }

    public Date deserialize(String day) {
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            return formatter.parse(day);
        } catch (ParseException e) {
            throw new IllegalArgumentException(String.format("Date value ('%s') in database was wrongly formatted!", day));
        }
    }
}
