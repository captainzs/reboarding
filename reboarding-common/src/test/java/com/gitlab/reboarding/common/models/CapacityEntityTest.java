package com.gitlab.reboarding.common.models;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Date;

@RunWith(JUnit4.class)
public class CapacityEntityTest {
    @Test
    public void givenOUT_whenGetter_assertSameReturned() {
        Integer capacity = 10;
        Date createdAt = new Date();
        CapacityEntity objectUnderTest = new CapacityEntity(capacity, createdAt);
        Assert.assertEquals(capacity, objectUnderTest.getCapacity());
        Assert.assertEquals(createdAt, objectUnderTest.getCreatedAt());
    }
}
