package com.gitlab.reboarding.common.models;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.Date;

@RunWith(JUnit4.class)
public class EnterEventEntityTest {
    @Test
    public void givenOUT_whenGetter_assertSameReturned() {
        RegistrationEntity reg = Mockito.mock(RegistrationEntity.class);
        Date createdAt = new Date();
        EnterEventEntity objectUnderTest = new EnterEventEntity(reg, createdAt);
        Assert.assertEquals(reg, objectUnderTest.getRegistration());
        Assert.assertEquals(createdAt, objectUnderTest.getCreatedAt());
    }
}
