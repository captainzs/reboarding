package com.gitlab.reboarding.common.models;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.Date;

@RunWith(JUnit4.class)
public class ExitEventEntityTest {
    @Test
    public void givenOUT_whenGetter_assertSameReturned() {
        EnterEventEntity entered = Mockito.mock(EnterEventEntity.class);
        Date createdAt = new Date();
        ExitEventEntity objectUnderTest = new ExitEventEntity(entered, createdAt);
        Assert.assertEquals(entered, objectUnderTest.getEnterEvent());
        Assert.assertEquals(createdAt, objectUnderTest.getCreatedAt());
    }
}
