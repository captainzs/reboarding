package com.gitlab.reboarding.common.models;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.Date;

@RunWith(JUnit4.class)
public class RegistrationEntityTest {
    @Test
    public void givenOUT_whenGetter_assertSameReturned() {
        SeatEntity seat = Mockito.mock(SeatEntity.class);
        String userId = "UserId";
        Date toDay = new Date();
        Date createdAt = new Date();
        RegistrationEntity objectUnderTest = new RegistrationEntity(seat, userId, toDay, createdAt);
        Assert.assertEquals(seat, objectUnderTest.getReservedSeat());
        Assert.assertEquals(userId, objectUnderTest.getUserId());
        Assert.assertEquals(toDay, objectUnderTest.getRegisteredTo());
        Assert.assertEquals(createdAt, objectUnderTest.getCreatedAt());
    }
}
