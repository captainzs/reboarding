package com.gitlab.reboarding.common.utils;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import java.util.Date;

@RunWith(JUnit4.class)
public class DateConverterTest {
    private DayConverter objectUnderTest;

    private final DaySerializer serializer = Mockito.mock(DaySerializer.class);
    private final DayDeserializer deserializer = Mockito.mock(DayDeserializer.class);

    @Before
    public void setUp() {
        this.objectUnderTest = new DayConverter(this.serializer, this.deserializer);
    }

    @Test
    public void givenDay_whenConvertToString_assertSerializerCalled() {
        Date day = new Date();
        this.objectUnderTest.convertToDatabaseColumn(day);
        Mockito.verify(this.serializer, Mockito.times(1)).serialize(day);
    }

    @Test
    public void givenDayStr_whenConvertToDate_assertDeserializerCalled() {
        String day = "2012-05-12";
        this.objectUnderTest.convertToEntityAttribute(day);
        Mockito.verify(this.deserializer, Mockito.times(1)).deserialize(day);
    }
}
