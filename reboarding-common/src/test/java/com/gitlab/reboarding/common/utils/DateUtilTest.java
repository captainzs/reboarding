package com.gitlab.reboarding.common.utils;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.ZonedDateTime;
import java.util.Date;

@RunWith(JUnit4.class)
public class DateUtilTest {
    @Test
    public void givenPastDay_whenIsDayInPast_assertTrue() {
        ZonedDateTime testDay = ZonedDateTime.now().minusDays(2);
        boolean result = DateUtil.isDayInPast(Date.from(testDay.toInstant()));
        Assert.assertTrue(result);
    }

    @Test
    public void givenToday_whenIsDayInPast_assertFalse() {
        ZonedDateTime testDay = ZonedDateTime.now();
        boolean result = DateUtil.isDayInPast(Date.from(testDay.toInstant()));
        Assert.assertFalse(result);
    }

    @Test
    public void givenTodayWithSmallTime_whenIsDayInPast_assertFalse() {
        ZonedDateTime testDay = ZonedDateTime.now().minusMinutes(1);
        boolean result = DateUtil.isDayInPast(Date.from(testDay.toInstant()));
        Assert.assertFalse(result);
    }
}
