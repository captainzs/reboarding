package com.gitlab.reboarding.common.utils;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Date;

@RunWith(JUnit4.class)
public class DaySerializerTest {
    private DaySerializer objectUnderTest;

    @Before
    public void setUp() {
        this.objectUnderTest = new DaySerializer();
    }

    @Test
    public void givenDay_whenSerialize_assertString() {
        Date day = new Date(15456746875L);
        Assert.assertEquals("1970-06-28", this.objectUnderTest.serialize(day));
    }
}
