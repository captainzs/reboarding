package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.common.models.CapacityEntity;
import com.gitlab.reboarding.common.utils.DateUtil;
import com.gitlab.reboarding.hr.bll.config.OfficeProperties;
import com.gitlab.reboarding.hr.dal.CapacityRepository;
import com.gitlab.reboarding.hr.models.CapacityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CapacityService {
    private final OfficeProperties properties;
    private final CapacityRepository repository;

    @Autowired
    public CapacityService(OfficeProperties properties, CapacityRepository repository) {
        this.properties = properties;
        this.repository = repository;
    }

    public int setCapacity(CapacityDto dto) {
        int capacity = this.calculateCapacity(dto.getPercentage());
        CapacityEntity entity = new CapacityEntity(capacity, DateUtil.now());
        return this.repository.save(entity).getCapacity();
    }

    private int calculateCapacity(int percentage) {
        return (int) Math.floor(this.properties.getMaxCapacity() * percentage / 100.0);
    }
}
