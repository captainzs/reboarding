package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.common.opencv.OfficeImageLoader;
import com.gitlab.reboarding.hr.models.AreaColor;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
public class OfficeImageProcessor {
    private static final double MIN_MATCH_PRECISION_RATE = 0.7;

    private final OfficeImageLoader imageLoader;

    @Autowired
    public OfficeImageProcessor(OfficeImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    public Set<Point> parseImageForSeats(Set<AreaColor> openAreas) throws IOException {
        Set<Point> allSeats = new HashSet<>();
        for (AreaColor color : openAreas) {
            Mat layout = this.filteredLayoutByArea(color);
            Set<Rect> matches1 = this.findMatches(layout, this.imageLoader.loadHorizontalTemplate());
            Set<Rect> matches2 = this.findMatches(layout, this.imageLoader.loadVerticalTemplate());
            Set<Set<Rect>> groups = RectUtil.groupMatches(Stream.of(matches1, matches2).flatMap(Set::stream).collect(Collectors.toSet()));
            allSeats.addAll(RectUtil.findCenterOfGroups(groups));
        }
        return allSeats;
    }

    private Mat filteredLayoutByArea(AreaColor color) throws IOException {
        double lowerHue;
        double upperHue;
        switch (color) {
            case GREEN:
                lowerHue = 40;
                upperHue = 70;
                break;
            case CYAN:
                lowerHue = 25;
                upperHue = 37;
                break;
            case PURPLE:
                lowerHue = 160;
                upperHue = 170;
                break;
            default:
                throw new UnsupportedOperationException(String.format("No office area supported with the color %s!", color));
        }
        Mat source = this.imageLoader.loadLayout();
        Mat hsvSource = new Mat();
        Imgproc.cvtColor(source, hsvSource, Imgproc.COLOR_RGB2HSV);
        Mat mask = new Mat();
        Core.inRange(hsvSource, new Scalar(lowerHue, 45, 240), new Scalar(upperHue, 255, 255), mask);
        Mat filteredSource = new Mat();
        Core.bitwise_and(source, source, filteredSource, mask);
        return filteredSource;
    }

    private Set<Rect> findMatches(Mat layout, Mat template) {
        Mat matchResult = new Mat();
        Imgproc.matchTemplate(layout, template, matchResult, Imgproc.TM_CCOEFF_NORMED);
        Set<Rect> matches = new HashSet<>();
        Core.MinMaxLocResult currentMmr = Core.minMaxLoc(matchResult);
        while (currentMmr.maxVal > MIN_MATCH_PRECISION_RATE) {
            Rect matchRect = new Rect(currentMmr.maxLoc, new Size(template.cols(), template.rows()));
            matches.add(matchRect);
            Imgproc.rectangle(matchResult, matchRect.tl(), matchRect.br(), new Scalar(0, 255, 0), -1);
            currentMmr = Core.minMaxLoc(matchResult);
        }
        return matches;
    }
}
