package com.gitlab.reboarding.hr.bll;

import org.opencv.core.Point;
import org.opencv.core.Rect;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

public class RectUtil {
    private static final double MIN_VALID_INTERSECTION_RATE = 0.2;

    public static Set<Set<Rect>> groupMatches(Set<Rect> matches) {
        Set<Set<Rect>> groups = new HashSet<>();
        for (Rect match : matches) {
            boolean intersects = false;
            for (Set<Rect> group : groups) {
                intersects = group.stream().anyMatch(s -> isMostlyIntersecting(match, s));
                if (intersects) {
                    group.add(match);
                    break;
                }
            }
            if (!intersects) {
                Set<Rect> newSeat = new HashSet<>();
                newSeat.add(match);
                groups.add(newSeat);
            }
        }
        return groups;
    }

    private static boolean isMostlyIntersecting(Rect r1, Rect r2) {
        Rectangle rec1 = new Rectangle(r1.x, r1.y, r1.width, r1.height);
        Rectangle rec2 = new Rectangle(r2.x, r2.y, r2.width, r2.height);
        Rectangle intersection = rec1.intersection(rec2);
        if (intersection.isEmpty()) {
            return false;
        }
        double intersectionSize = intersection.width * intersection.height;
        double rec1Size = rec1.width * rec1.height;
        double rec2Size = rec2.width * rec2.height;
        double recAvgSize = (rec1Size + rec2Size) / 2.0;
        return intersectionSize > recAvgSize * MIN_VALID_INTERSECTION_RATE;
    }

    public static Set<Point> findCenterOfGroups(Set<Set<Rect>> groups) {
        Set<Point> centerOfGroups = new HashSet<>(groups.size());
        for (Set<Rect> group : groups) {
            double sumOfCenterPosX = 0;
            double sumOfCenterPosY = 0;
            for (Rect rect : group) {
                double centerX = rect.x + (rect.width / 2.0);
                double centerY = rect.y + (rect.height / 2.0);
                sumOfCenterPosX += centerX;
                sumOfCenterPosY += centerY;
            }
            centerOfGroups.add(new Point(sumOfCenterPosX / group.size(), sumOfCenterPosY / group.size()));
        }
        return centerOfGroups;
    }
}
