package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.common.models.RegistrationEntity;
import com.gitlab.reboarding.common.models.SeatEntity;
import com.gitlab.reboarding.common.models.SeatStatus;
import com.gitlab.reboarding.common.opencv.ImageDrawer;
import com.gitlab.reboarding.common.utils.DateUtil;
import com.gitlab.reboarding.hr.dal.SeatRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

@Service
public class ReservationService {
    private final ImageDrawer imageDrawer;
    private final SeatRepository repository;

    @Autowired
    public ReservationService(ImageDrawer imageDrawer, SeatRepository repository) {
        this.imageDrawer = imageDrawer;
        this.repository = repository;
    }

    public ByteArrayResource getOfficeWithCurrentReservationStatuses() throws IOException {
        Date day = DateUtil.now();
        Map<SeatEntity, SeatStatus> seatStatuses = new HashMap<>();
        List<SeatEntity> allSeats = this.repository.findAll();
        for (SeatEntity seat : allSeats) {
            SeatStatus status = null;
            Optional<RegistrationEntity> registration = seat.getActiveRegistration(day);
            if (registration.isPresent() && registration.get().isEntered()) {
                status = SeatStatus.RESERVED;
            } else if (registration.isPresent() && !registration.get().isEntered()) {
                status = SeatStatus.REGISTERED;
            } else if (seat.isInSafeArea(day)) {
                status = SeatStatus.FREE;
            }
            if (status != null) {
                seatStatuses.put(seat, status);
            }
        }
        return this.imageDrawer.createOfficeMapWithSeatStatuses(seatStatuses);
    }
}
