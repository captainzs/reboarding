package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.common.models.SeatEntity;
import com.gitlab.reboarding.hr.bll.config.OfficeProperties;
import com.gitlab.reboarding.hr.dal.SeatRepository;
import org.opencv.core.Point;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Set;

@Component
public class SeatInitializer {
    private final OfficeImageProcessor processor;
    private final SeatMapAnalyzer conflictSearcher;
    private final SeatRepository repository;
    private final OfficeProperties properties;

    @Autowired
    public SeatInitializer(OfficeImageProcessor processor, SeatMapAnalyzer conflictSearcher, SeatRepository repository, OfficeProperties properties) {
        this.processor = processor;
        this.conflictSearcher = conflictSearcher;
        this.repository = repository;
        this.properties = properties;
    }

    @PostConstruct
    public void processAvailableSeats() throws IOException {
        Set<Point> seatPoints = this.processor.parseImageForSeats(this.properties.getOpenAreas());
        Set<SeatEntity> seats = this.conflictSearcher.makeSeatMap(seatPoints, this.properties.getMinDistanceMetres() * 10);
        this.repository.saveAll(seats);
    }
}
