package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.common.models.SeatEntity;
import org.opencv.core.Point;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class SeatMapAnalyzer {
    public Set<SeatEntity> makeSeatMap(Set<Point> seatPoints, int minPixelDistance) {
        Set<SeatEntity> seats = this.convertToObjects(seatPoints);
        for (SeatEntity seat : seats) {
            Set<SeatEntity> conflicts = new HashSet<>();
            for (SeatEntity otherSeat : seats) {
                if (seat != otherSeat) {
                    if (this.distance(seat, otherSeat) < minPixelDistance) {
                        conflicts.add(otherSeat);
                    }
                }
            }
            seat.setInConflictWith(conflicts);
        }
        return seats;
    }

    private Set<SeatEntity> convertToObjects(Set<Point> seatPoints) {
        Set<SeatEntity> seats = new HashSet<>();
        for (Point seatCenter : seatPoints) {
            seats.add(new SeatEntity(seatCenter.x, seatCenter.y));
        }
        return seats;
    }

    private double distance(SeatEntity seat1, SeatEntity seat2) {
        return Math.sqrt(Math.pow(seat2.getPosX() - seat1.getPosX(), 2) + Math.pow(seat2.getPosY() - seat1.getPosY(), 2));
    }
}
