package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.common.models.VipEntity;
import com.gitlab.reboarding.hr.bll.config.VipProperties;
import com.gitlab.reboarding.hr.dal.VipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

@Component
public class VipPersister {
    private final VipRepository repository;
    private final VipProperties properties;

    @Autowired
    public VipPersister(VipRepository repository, VipProperties properties) {
        this.repository = repository;
        this.properties = properties;
    }

    @PostConstruct
    public void persistAllVipNames() {
        Set<VipEntity> allVips = new HashSet<>();
        for (String name : this.properties.getUserIds()) {
            allVips.add(new VipEntity(name));
        }
        this.repository.saveAll(allVips);
    }
}
