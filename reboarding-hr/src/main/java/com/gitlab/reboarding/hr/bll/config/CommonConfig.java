package com.gitlab.reboarding.hr.bll.config;

import com.gitlab.reboarding.common.opencv.ImageDrawer;
import com.gitlab.reboarding.common.opencv.OfficeImageLoader;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration("HrCommonConfig")
public class CommonConfig {
    @Bean
    @ConditionalOnMissingBean(OfficeImageLoader.class)
    public OfficeImageLoader imageLoader() {
        return new OfficeImageLoader();
    }

    @Bean
    @ConditionalOnMissingBean(ImageDrawer.class)
    public ImageDrawer imageDrawerBean(OfficeImageLoader imageLoader) {
        return new ImageDrawer(imageLoader);
    }
}
