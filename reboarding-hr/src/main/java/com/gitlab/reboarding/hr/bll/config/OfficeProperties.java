package com.gitlab.reboarding.hr.bll.config;

import com.gitlab.reboarding.hr.models.AreaColor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Component
@Validated
@ConfigurationProperties(prefix = "office")
public class OfficeProperties {
    @NotNull
    @Min(0)
    private Integer maxCapacity;
    @NotNull
    @Min(1)
    @Min(5)
    private Integer minDistanceMetres;
    @NotNull
    private Set<AreaColor> openAreas;

    public synchronized Integer getMaxCapacity() {
        return this.maxCapacity;
    }

    public synchronized void setMaxCapacity(Integer maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public synchronized Integer getMinDistanceMetres() {
        return this.minDistanceMetres;
    }

    public synchronized void setMinDistanceMetres(Integer minDistanceMetres) {
        this.minDistanceMetres = minDistanceMetres;
    }

    public synchronized Set<AreaColor> getOpenAreas() {
        return this.openAreas;
    }

    public synchronized void setOpenAreas(Set<AreaColor> openAreas) {
        this.openAreas = openAreas;
    }
}
