package com.gitlab.reboarding.hr.bll.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Component
@Validated
@ConfigurationProperties(prefix = "vip")
public class VipProperties {
    @NotNull
    private Set<String> userIds;

    public synchronized Set<String> getUserIds() {
        return this.userIds;
    }

    public synchronized void setUserIds(Set<String> userIds) {
        this.userIds = userIds;
    }
}
