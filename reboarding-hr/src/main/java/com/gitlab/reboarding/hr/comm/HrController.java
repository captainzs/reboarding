package com.gitlab.reboarding.hr.comm;

import com.gitlab.reboarding.hr.bll.CapacityService;
import com.gitlab.reboarding.hr.bll.ReservationService;
import com.gitlab.reboarding.hr.models.CapacityDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping(value = "/hr")
public class HrController {
    private final CapacityService service;
    private final ReservationService reservationService;

    @Autowired
    public HrController(CapacityService service, ReservationService reservationService) {
        this.service = service;
        this.reservationService = reservationService;
    }

    @PostMapping(value = "/capacity")
    public Integer setCapacity(@RequestBody @Valid CapacityDto capacity) {
        return this.service.setCapacity(capacity);
    }

    @GetMapping(value = "/layout")
    public ResponseEntity<Resource> getLayoutReservation() throws IOException {
        ByteArrayResource resource = this.reservationService.getOfficeWithCurrentReservationStatuses();
        return ResponseEntity.ok()
                .contentLength(resource.contentLength())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }
}
