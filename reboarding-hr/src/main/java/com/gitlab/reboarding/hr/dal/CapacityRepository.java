package com.gitlab.reboarding.hr.dal;

import com.gitlab.reboarding.common.models.CapacityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CapacityRepository extends JpaRepository<CapacityEntity, Long> {
}
