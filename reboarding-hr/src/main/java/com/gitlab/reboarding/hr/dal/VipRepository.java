package com.gitlab.reboarding.hr.dal;

import com.gitlab.reboarding.common.models.VipEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VipRepository extends JpaRepository<VipEntity, Long> {
}
