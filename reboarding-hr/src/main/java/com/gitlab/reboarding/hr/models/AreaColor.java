package com.gitlab.reboarding.hr.models;

public enum AreaColor {
    GREEN, CYAN, PURPLE
}
