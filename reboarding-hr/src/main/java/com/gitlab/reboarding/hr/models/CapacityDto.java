package com.gitlab.reboarding.hr.models;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class CapacityDto {
    @NotNull
    @Min(0)
    @Max(100)
    private Integer percentage;

    private CapacityDto() {
    }

    public CapacityDto(Integer percentage) {
        this.percentage = percentage;
    }

    public Integer getPercentage() {
        return this.percentage;
    }
}
