package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.common.models.CapacityEntity;
import com.gitlab.reboarding.hr.bll.config.OfficeProperties;
import com.gitlab.reboarding.hr.dal.CapacityRepository;
import com.gitlab.reboarding.hr.models.CapacityDto;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CapacityServiceTest {
    private CapacityService objectUnderTest;

    @Mock
    private OfficeProperties properties;

    @Mock
    private CapacityRepository repository;

    @Before
    public void setUp() {
        this.objectUnderTest = new CapacityService(this.properties, this.repository);
    }

    @Test
    public void given250Seats_when10PercentCapacity_assert25SeatsFree() {
        Mockito.when(this.properties.getMaxCapacity()).thenReturn(250);
        CapacityDto input = Mockito.mock(CapacityDto.class);
        Mockito.when(input.getPercentage()).thenReturn(10);

        CapacityEntity record = Mockito.mock(CapacityEntity.class);
        Mockito.when(this.repository.save(Mockito.any())).thenReturn(record);

        this.objectUnderTest.setCapacity(input);

        ArgumentCaptor<CapacityEntity> captor = ArgumentCaptor.forClass(CapacityEntity.class);
        Mockito.verify(this.repository, Mockito.times(1)).save(captor.capture());
        Assert.assertEquals(25, captor.getValue().getCapacity().intValue());
    }

    @Test
    public void given200Seats_when3PercentCapacity_assert6SeatsFree() {
        Mockito.when(this.properties.getMaxCapacity()).thenReturn(210);
        CapacityDto input = Mockito.mock(CapacityDto.class);
        Mockito.when(input.getPercentage()).thenReturn(3);

        CapacityEntity record = Mockito.mock(CapacityEntity.class);
        Mockito.when(this.repository.save(Mockito.any(CapacityEntity.class))).thenReturn(record);

        this.objectUnderTest.setCapacity(input);

        ArgumentCaptor<CapacityEntity> captor = ArgumentCaptor.forClass(CapacityEntity.class);
        Mockito.verify(this.repository, Mockito.times(1)).save(captor.capture());
        Assert.assertEquals(6, captor.getValue().getCapacity().intValue());
    }
}
