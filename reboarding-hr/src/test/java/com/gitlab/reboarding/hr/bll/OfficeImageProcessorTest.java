package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.common.opencv.OfficeImageLoader;
import com.gitlab.reboarding.hr.models.AreaColor;
import nu.pattern.OpenCV;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.core.Point;
import org.opencv.imgcodecs.Imgcodecs;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@RunWith(MockitoJUnitRunner.class)
public class OfficeImageProcessorTest {
    private OfficeImageProcessor objectUnderTest;

    @Mock
    private OfficeImageLoader imageLoader;

    static {
        OpenCV.loadShared();
    }

    @Before
    public void before() throws IOException {
        Mat layout = this.loadTestResource("original_layout.jpg");
        Mat horizontalSeat = this.loadTestResource("template_horizontal.png");
        Mat verticalSeat = this.loadTestResource("template_vertical.png");
        Mockito.when(this.imageLoader.loadLayout()).thenReturn(layout);
        Mockito.when(this.imageLoader.loadHorizontalTemplate()).thenReturn(horizontalSeat);
        Mockito.when(this.imageLoader.loadVerticalTemplate()).thenReturn(verticalSeat);
        this.objectUnderTest = new OfficeImageProcessor(this.imageLoader);
    }

    private Mat loadTestResource(String fileName) throws IOException {
        InputStream stream = this.getClass().getClassLoader().getResourceAsStream(fileName);
        byte[] data = StreamUtils.copyToByteArray(stream);
        return Imgcodecs.imdecode(new MatOfByte(data), Imgcodecs.IMREAD_UNCHANGED);
    }

    @Test
    public void givenTestImagesAndGreenArea_whenParseImage_assertAllSeatsFound() throws IOException {
        Set<Point> seats = this.objectUnderTest.parseImageForSeats(Collections.singleton(AreaColor.GREEN));
        Assert.assertEquals(192, seats.size());
    }

    @Test
    public void givenTestImagesAndCyanArea_whenParseImage_assertAllSeatsFound() throws IOException {
        Set<Point> seats = this.objectUnderTest.parseImageForSeats(Collections.singleton(AreaColor.CYAN));
        Assert.assertEquals(37, seats.size());
    }

    @Test
    public void givenTestImagesAndPurpleArea_whenParseImage_assertAllSeatsFound() throws IOException {
        Set<Point> seats = this.objectUnderTest.parseImageForSeats(Collections.singleton(AreaColor.PURPLE));
        Assert.assertEquals(24, seats.size());
    }

    @Test
    public void givenTestImagesAndAllColorArea_whenParseImage_assertAllSeatsFound() throws IOException {
        Set<AreaColor> allColor = new HashSet<>();
        allColor.add(AreaColor.PURPLE);
        allColor.add(AreaColor.CYAN);
        allColor.add(AreaColor.GREEN);
        Set<Point> seats = this.objectUnderTest.parseImageForSeats(allColor);
        Assert.assertEquals(192 + 37 + 24, seats.size());
    }
}
