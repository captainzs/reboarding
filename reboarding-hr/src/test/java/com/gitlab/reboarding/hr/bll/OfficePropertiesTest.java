package com.gitlab.reboarding.hr.bll;

import com.gitlab.reboarding.hr.bll.config.OfficeProperties;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class OfficePropertiesTest {
    private OfficeProperties objectUnderTest;

    @Before
    public void setUp() {
        this.objectUnderTest = new OfficeProperties();
    }

    @Test
    public void givenMaxSeats_whenGet_assertSame() {
        int maxSeats = 250;
        this.objectUnderTest.setMaxCapacity(maxSeats);
        Assert.assertEquals(250, this.objectUnderTest.getMaxCapacity().intValue());
    }
}
