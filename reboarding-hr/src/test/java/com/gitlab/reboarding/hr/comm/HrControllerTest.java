package com.gitlab.reboarding.hr.comm;

import com.gitlab.reboarding.hr.bll.CapacityService;
import com.gitlab.reboarding.hr.bll.ReservationService;
import com.gitlab.reboarding.hr.models.CapacityDto;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class HrControllerTest {
    private HrController objectUnderTest;

    @Mock
    private CapacityService service;
    @Mock
    private ReservationService reservationService;

    @Before
    public void setUp() {
        this.objectUnderTest = new HrController(this.service, this.reservationService);
    }

    @Test
    public void givenInputDto_whenSetCapacity_verifyServiceCalled() {
        CapacityDto input = Mockito.mock(CapacityDto.class);
        this.objectUnderTest.setCapacity(input);
        Mockito.verify(this.service, Mockito.times(1)).setCapacity(input);
    }
}
