package com.gitlab.reboarding.hr.models;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CapacityDtoTest {
    @Test
    public void givenPercentage_whenGet_assertSame() {
        int percentage = 15;
        CapacityDto dto = new CapacityDto(percentage);
        Assert.assertEquals(percentage, dto.getPercentage().intValue());
    }
}
