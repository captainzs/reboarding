package com.gitlab.reboarding.test;

import nu.pattern.OpenCV;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableRetry
@EntityScan("com.gitlab.reboarding.common.models")
@EnableSwagger2
@EnableTransactionManagement
@EnableJpaRepositories("com.gitlab.reboarding.booking.dal")
@SpringBootApplication(scanBasePackages = {
        "com.gitlab.reboarding.booking.bll",
        "com.gitlab.reboarding.booking.comm",
        "com.gitlab.reboarding.booking.dal",
        "com.gitlab.reboarding.booking.models"})
public class BookingApplication {
    static {
        OpenCV.loadShared();
    }
}
