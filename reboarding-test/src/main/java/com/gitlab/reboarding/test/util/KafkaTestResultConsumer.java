package com.gitlab.reboarding.test.util;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@EnableBinding(Sink.class)
public class KafkaTestResultConsumer {
    private final List<String> consumedMessages = new ArrayList<>();

    public List<String> getConsumedMessages() {
        return new ArrayList<>(this.consumedMessages);
    }

    @StreamListener(Sink.INPUT)
    private void listerForMessages(String userId) {
        this.consumedMessages.add(userId);
    }
}
