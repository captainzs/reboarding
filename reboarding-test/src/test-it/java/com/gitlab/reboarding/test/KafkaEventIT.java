package com.gitlab.reboarding.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.reboarding.booking.models.RegisterDto;
import com.gitlab.reboarding.common.utils.DateUtil;
import com.gitlab.reboarding.hr.models.CapacityDto;
import com.gitlab.reboarding.test.util.KafkaTestResultConsumer;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@RunWith(SpringRunner.class)
@EmbeddedKafka(ports = 9092)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@SpringBootTest(classes = {HrApplication.class, BookingApplication.class, KafkaTestResultConsumer.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "spring.datasource.url=jdbc:h2:mem:testdb;MV_STORE=FALSE",
                "spring.datasource.driverClassName=org.h2.Driver",
                "spring.datasource.username=sa",
                "spring.datasource.password=sa",
                "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect",
                "spring.jpa.hibernate.ddl-auto=create",
                "office.open-areas=GREEN",
                "office.max-capacity=250",
                "office.min-distance-metres=5",
                "vip.user-ids=Obama",
                "spring.cloud.stream.kafka.binder.brokers=localhost",
                "spring.cloud.stream.bindings.input.group=test-client",
                "spring.cloud.stream.bindings.input.destination=events"})
public class KafkaEventIT {
    @Autowired
    private MockMvc mockClient;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private KafkaTestResultConsumer resultConsumer;

    @Test
    public void given2Capacity_when6EmployeesRegister_assertLastOneGetsKafkaEventWhenSomebodyExits() throws Exception {
        MvcResult capacityResult = this.mockClient.perform(
                MockMvcRequestBuilders.post("/hr/capacity")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(new CapacityDto(1))))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        Assert.assertEquals(2, Integer.valueOf(capacityResult.getResponse().getContentAsString()).intValue());

        for (int i = 1; i <= 6; i++) {
            MvcResult result = this.mockClient.perform(
                    MockMvcRequestBuilders.post("/employee/register")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(this.objectMapper.writeValueAsString(new RegisterDto("user" + i, DateUtil.now()))))
                    .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
            String resultStrJson = result.getResponse().getContentAsString();
            JSONObject jsonObject = new JSONObject(resultStrJson);
            Assert.assertEquals("user" + i, jsonObject.getString("userId"));
            Assert.assertFalse(jsonObject.getBoolean("canRegister"));
            Assert.assertTrue(jsonObject.getLong("seatId") > 0);
        }

        this.mockClient.perform(
                MockMvcRequestBuilders.post("/terminal/enter")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("user1"))
                .andExpect(MockMvcResultMatchers.status().isOk());
        this.mockClient.perform(
                MockMvcRequestBuilders.post("/terminal/exit")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("user1"))
                .andExpect(MockMvcResultMatchers.status().isOk());

        Thread.sleep(1000);

        Assert.assertEquals(1, this.resultConsumer.getConsumedMessages().size());
        Assert.assertEquals("user6", this.resultConsumer.getConsumedMessages().get(0));
    }
}
