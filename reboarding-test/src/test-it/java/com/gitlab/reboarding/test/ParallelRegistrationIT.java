package com.gitlab.reboarding.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.reboarding.booking.models.RegisterDto;
import com.gitlab.reboarding.common.utils.DateUtil;
import com.gitlab.reboarding.hr.models.CapacityDto;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;


// H2 not supports multi thread environments
// This test shall be run with production database
@Ignore
@RunWith(SpringRunner.class)
@EmbeddedKafka(ports = 9092)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@SpringBootTest(classes = {HrApplication.class, BookingApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "spring.datasource.url=jdbc:postgresql://postgres:5432/postgres",
                "spring.datasource.username=postgres",
                "spring.datasource.password=pass",
                "spring.jpa.hibernate.ddl-auto=create-drop",
                "office.open-areas=GREEN",
                "office.max-capacity=250",
                "office.min-distance-metres=5",
                "vip.user-ids=Obama"})
public class ParallelRegistrationIT {
    @Autowired
    private MockMvc mockClient;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenFullCapacity_whenEmployeesRegisteringParallel_assertAllGetsDifferentSeat() throws Exception {
        MvcResult result = this.mockClient.perform(
                MockMvcRequestBuilders.post("/hr/capacity")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(new CapacityDto(100))))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        Assert.assertEquals(250, Integer.valueOf(result.getResponse().getContentAsString()).intValue());

        ExecutorService executor = Executors.newCachedThreadPool();

        Callable<Integer> callableTask = () -> {
            MvcResult taskResult = this.mockClient.perform(
                    MockMvcRequestBuilders.post("/employee/register")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(this.objectMapper.writeValueAsString(new RegisterDto(UUID.randomUUID().toString(), DateUtil.now()))))
                    .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
            String resultStrJson = taskResult.getResponse().getContentAsString();
            System.out.println(resultStrJson);
            JSONObject jsonObject = new JSONObject(resultStrJson);
            return jsonObject.getInt("seatId");
        };

        List<Callable<Integer>> tasks = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            tasks.add(callableTask);
        }
        List<Future<Integer>> results = executor.invokeAll(tasks);

        Set<Integer> seatsIds = new HashSet<>(results.size());
        for (Future<Integer> future : results) {
            seatsIds.add(future.get());
        }
        Assert.assertEquals(5, seatsIds.size());
    }
}
