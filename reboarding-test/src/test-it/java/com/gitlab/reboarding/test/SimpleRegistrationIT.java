package com.gitlab.reboarding.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.reboarding.booking.models.RegisterDto;
import com.gitlab.reboarding.common.utils.DateUtil;
import com.gitlab.reboarding.hr.models.CapacityDto;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.MockMvcPrint;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;


@RunWith(SpringRunner.class)
@EmbeddedKafka(ports = 9092)
@AutoConfigureMockMvc(print = MockMvcPrint.NONE)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
@SpringBootTest(classes = {HrApplication.class, BookingApplication.class},
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {
                "spring.datasource.url=jdbc:h2:mem:testdb;MV_STORE=FALSE",
                "spring.datasource.driverClassName=org.h2.Driver",
                "spring.datasource.username=sa",
                "spring.datasource.password=sa",
                "spring.jpa.database-platform=org.hibernate.dialect.H2Dialect",
                "spring.jpa.hibernate.ddl-auto=create",
                "office.open-areas=GREEN",
                "office.max-capacity=250",
                "office.min-distance-metres=5",
                "vip.user-ids=Obama"})
public class SimpleRegistrationIT {
    @Autowired
    private MockMvc mockClient;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void given2Capacity_when3UserRegisters_assertThirdOneHasToWait() throws Exception {
        MvcResult result = this.mockClient.perform(
                MockMvcRequestBuilders.post("/hr/capacity")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(new CapacityDto(1))))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andReturn();
        Assert.assertEquals(2, Integer.valueOf(result.getResponse().getContentAsString()).intValue());

        MvcResult user1Result = this.mockClient.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(new RegisterDto("user1", DateUtil.now()))))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        String resultStrJson = user1Result.getResponse().getContentAsString();
        JSONObject jsonObject = new JSONObject(resultStrJson);
        Assert.assertEquals("user1", jsonObject.getString("userId"));
        Assert.assertTrue(jsonObject.getBoolean("canEnter"));

        MvcResult user2Result = this.mockClient.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(new RegisterDto("user2", DateUtil.now()))))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        resultStrJson = user2Result.getResponse().getContentAsString();
        jsonObject = new JSONObject(resultStrJson);
        Assert.assertEquals("user2", jsonObject.getString("userId"));
        Assert.assertTrue(jsonObject.getBoolean("canEnter"));

        MvcResult user3Result = this.mockClient.perform(
                MockMvcRequestBuilders.post("/employee/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(this.objectMapper.writeValueAsString(new RegisterDto("user3", DateUtil.now()))))
                .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();
        resultStrJson = user3Result.getResponse().getContentAsString();
        jsonObject = new JSONObject(resultStrJson);
        Assert.assertEquals("user3", jsonObject.getString("userId"));
        Assert.assertEquals(1, jsonObject.getInt("waitNo"));
        Assert.assertFalse(jsonObject.getBoolean("canEnter"));
    }
}
