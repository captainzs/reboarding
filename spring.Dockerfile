FROM openjdk:11

ARG JAR_FILE

COPY $JAR_FILE /opt/spring-app.jar

WORKDIR /opt
ENTRYPOINT ["java","-jar","spring-app.jar"]